import hashlib
import os
import random

from django.conf import settings

import numpy as np
import pandas as pd
from requests import request


def get_sha1_hash(text):
    hash_object = hashlib.sha1()
    hash_object.update(text.encode('utf-8'))
    return hash_object.hexdigest().upper()


def get_regions():
    RAPIDAPI_ALL_URL = os.sep.join([settings.RAPIDAPI['url'], 'all'])

    try:
        response = request('GET', RAPIDAPI_ALL_URL, headers=settings.RAPIDAPI['headers'])
        response.raise_for_status()
        rapidapi_json = response.json()
        return set([item['region'] for item in rapidapi_json if item['region'] != ''])

    except Exception as e:
        raise e


def get_random_country_by_region(region):
    REST_COUNTRIES_BY_REGION_URL = os.sep.join([settings.REST_COUNTRIES['url'], 'region', region])

    try:
        response = request('GET', REST_COUNTRIES_BY_REGION_URL)
        response.raise_for_status()
        country_json = random.choices(response.json()).pop()
        return country_json
    except Exception as e:
        raise e


def get_statistics(countries):

    countries_dataframe = pd.DataFrame(
        {'time': [country.generate_time for country in countries]},
        index=[country.city for country in countries]
    )

    return countries_dataframe['time'].agg(['min','max','mean', 'sum'])