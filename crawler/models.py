from django.db import models

from .utils import get_sha1_hash


class RequestData(models.Model):

    created = models.DateTimeField(
        auto_now_add=True
    )


class CountryData(models.Model):

    region = models.CharField(
        max_length=50,

    )

    city = models.CharField(
        max_length=100,
    )

    language = models.CharField(
        max_length=100,
    )

    generate_time = models.FloatField()

    request_data = models.ForeignKey(
        'crawler.RequestData',
        on_delete=models.CASCADE
    )


    @property
    def language_hash(self):
        if not hasattr(self, '_language_hash'):
            self._language_hash = get_sha1_hash(self.language)

        return self._language_hash