from time import time

from django.shortcuts import render

from .models import RequestData
from .models import CountryData
from .utils import get_regions
from .utils import get_random_country_by_region
from .utils import get_sha1_hash
from .utils import get_statistics



def index(request):
    countries = []
    regions = get_regions()

    request_data = RequestData.objects.create()

    for region in regions:
        init_time = time()
        country_json = get_random_country_by_region(region)
        # Gap of country data request
        row_gap = time() - init_time

        countries.append(
            CountryData.objects.create(
                region=region,
                city=country_json['capital'],
                language=country_json['languages'][0]['name'],
                generate_time=row_gap,
                request_data=request_data
            )
        )

    min_time, max_time, mean_time, total_time = get_statistics(countries)

    return render(
        request,
        'crawler/index.html',
        {
            'countries': countries,
            'min_time': min_time,
            'max_time': max_time,
            'mean_time': mean_time,
            'total_time': total_time,
        }
    )